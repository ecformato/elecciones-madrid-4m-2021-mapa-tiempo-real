//Uso de librería d3
import { json } from 'd3-request';
import { queue } from 'd3-queue';
import { select, event } from 'd3-selection';
import { geoPath } from 'd3-geo';
import { geoConicConformalSpain } from 'd3-composite-projections';
import { feature } from 'topojson-client';
import { zoom as zoomInit, zoomIdentity } from 'd3-zoom';

import { initAutocomplete } from '../helpers/autocomplete';
import { comparativa2019, comparativa2021 } from '../helpers/dom-logic';
import { superPartyBlockId, CURRENTELECTION_ID, FORMERELECTION_ID } from '../helpers/parties';
import { scaleLinear } from 'd3-scale';

export let posicionInicial, zoomCity;

//CONSTANTES
const CHANGEDOWN_PARTIES = ['#f08e89', '#f0afa9', '#f1cfca']; // De - infinito a 0
const CHANGEUP_PARTIES = ['#b0c4c4','#6f979d','#2e6b77']; // De 0 a infinito
const DRAW = '#f1f0ea';
const GRADIENT_IDEOLOGY = {
    'izquierda': {first: '#f2edd1', second: '#f4eab8', third: '#f5e79f', fourth: '#f6e385', fifth: '#fbdd49', sixth: '#e2c32a'},
    'derecha': {first: '#d3d3d1', second: '#b5b7b8', third: '#979b9f', fourth: '#787e86', fifth: '#5a626d', sixth: '#3c4554'}
}
const WITHOUTDATA = '#e1e1e1';

export async function desktopDevelopment() {
    let zoomed, currentZoom = 0, stopped; //currentZoom > Para dejar de tener un municipio seleccionado
    let map, currentCityZoomed, svg, g, zoom, currentDataVisible;
    let mapWidth = document.getElementById('map').clientWidth, mapHeight = 350;
    let mapFeatures = [], arrayNombreMunicipios = [];
    let tooltip;
    let participacion, escrutado;
    let citySelected = '';

    let currentButton = 'primeraFuerza';
    let currentFilter = 'year2021';

    ///////
    //// CARGA DE INFORMACIÓN
    ///////
    uploadEveryData().then(function(){
        initEvents();
        initZoomEvents();
    });
    uploadGeneralProps();

    async function uploadEveryData() {
        let q = queue();
        //Listado de municipios para autocompletado
        q.defer(json, "https://raw.githubusercontent.com/CarlosMunozDiazEC/prueba-datos/main/batalla_madrid/mun_madrid_data.json");
        //Mapa de municipios
        q.defer(json, "https://raw.githubusercontent.com/CarlosMunozDiazEC/prueba-datos/main/batalla_madrid/mun_madrid_topo.json");
        //Llamadas datos para mapa
        q.defer(json, `https://api.elconfidencial.com/service/electionsscytl/map/${CURRENTELECTION_ID}/5/CA13/`); //2021
        q.defer(json, `https://api.elconfidencial.com/service/electionsscytl/map/${FORMERELECTION_ID}/5/CA13/`); //2019

        q.await(function(error, nombreMunicipios, poligonos, datos2021, datos2019){
            if(error) throw initEmptyMap();

            //Previo al mapa > Tooltip y buscador
            arrayNombreMunicipios = [...nombreMunicipios];
            initData();

            /* 
            * MAPA VACÍO 
            */
            function initEmptyMap() {
                let btn = document.getElementsByClassName('btn');
                for(let i = 0; i < btn.length; i++) {
                    btn[i].style.background = '#ccc';
                    btn[i].style.color = '#000';
                    btn[i].style.fontWeight = 'normal';
                }

                json("https://raw.githubusercontent.com/CarlosMunozDiazEC/prueba-datos/main/batalla_madrid/mun_madrid_topo.json", function (poligonos){
                    map = select("#map");
                    currentCityZoomed = '';
                
                    svg = map.append("svg")
                    .attr("width", mapWidth)
                    .attr("height", mapHeight)
                    .on("click", stopped, true);

                    //Polígonos municipales
                    let es = poligonos;
                    let us = feature(es, es.objects.mun_madrid);            
                    
                    const projection = geoConicConformalSpain().scale(1800).fitSize([mapWidth,mapHeight], us);
                    const path = geoPath(projection);
                    
                    mapFeatures = [...us.features];

                    g = svg
                        .append('g')
                        .attr('id', 'gmap');
            
                    //Estado inicial > Primera fuerza de 2021
                    g.selectAll("path")
                        .data(mapFeatures)
                        .enter()
                        .append("path")
                        .attr("d", path)
                        .attr("class", 'madrid-mun')
                        .attr("id", function(d){ return d.id; })
                        .style("fill", function(){
                            return WITHOUTDATA;                    
                        })
                        .style('opacity', 1)
                        .style("stroke", "#fff")
                        .style("stroke-width", "0.5px");
                });               
            }

            /*
            * MAPA
            */            
            map = select("#map");
            currentCityZoomed = '';
        
            svg = map.append("svg")
                .attr("width", mapWidth)
                .attr("height", mapHeight)
                .on("click", stopped, true);
        
            ///////
            //// DIBUJO INICIAL DEL MAPA
            ///////
        
            //Polígonos municipales
            let es = poligonos;
            let us = feature(es, es.objects.mun_madrid);            
            
            const projection = geoConicConformalSpain().scale(1800).fitSize([mapWidth,mapHeight], us);
            const path = geoPath(projection);
            
            mapFeatures = [...us.features];

            let apiData = datos2021.data;
            let apiData2019 = datos2019.data;
            
            mapFeatures.forEach(function(item){
                item.properties.centroid = path.centroid(item);
                //2021
                let api2021 = apiData.filter(function(itemApi) {
                    if(item.id == itemApi.id) {
                        return itemApi;
                    }
                })[0];
                item.api2021 = api2021;
                //2019
                let api2019 = apiData2019.filter(function(itemApi) {
                    if(item.id == itemApi.id) {
                        return itemApi;
                    }
                })[0];
                item.api2019 = api2019;

                //Comprobación y comparativa para participación
                if(item.api2021) {                    
                    item.participation2021 = item.api2021["participation "].par_percent_total_votes;
                    item.difParticipacion = item.api2021["participation "].par_percent_total_votes - item.api2019["participation "].par_percent_total_votes;                    

                    //Comprobación y comparativa para partidos y bloques ideológicos
                    if(item.api2021.parties.length > 0) {      
                        ////// BLOQUE IDEOLOGICO //////
                        let izquierdaPorc2021 = 0, derechaPorc2021 = 0, restoPorc2021 = 0;
                        for (let i = 0; i < item.api2021.parties.length; i++) {
                            let party = item.api2021.parties[i];

                            let tipoBloque = superPartyBlockId[party.par_meta_id] ? superPartyBlockId[party.par_meta_id].bloque : 'sin-bloque';

                            if (tipoBloque == 'izquierda') {
                                izquierdaPorc2021 += party.par_percent;
                            } else if (tipoBloque == 'derecha') {
                                derechaPorc2021 += party.par_percent;
                            } else {
                                restoPorc2021 += party.par_percent;
                            }

                            item.bloqueGanador2021 = izquierdaPorc2021 > derechaPorc2021 ? 'izquierda' : izquierdaPorc2021 < derechaPorc2021 ? 'derecha' : 'empate';
                            item.izquierdaPorc2021 = izquierdaPorc2021;
                            item.derechaPorc2021 = derechaPorc2021;
                            item.restoPorc2021 = restoPorc2021;
                        }

                        ////// PARTIDOS POLÍTICOS //////
                        //Cambio PSOE
                        let psoe2021Exist = 0, psoe2019Exist = 0, difPsoe = 0;

                        psoe2021Exist = item.api2021.parties ? 
                                        item.api2021.parties.filter(item => item.par_meta_id == 40)[0] ? 
                                        item.api2021.parties.filter(item => item.par_meta_id == 40)[0] : -1 : -1;

                        psoe2019Exist = item.api2019.parties ? 
                                        item.api2019.parties.filter(item => item.par_meta_id == 40)[0] ? 
                                        item.api2019.parties.filter(item => item.par_meta_id == 40)[0] : -1 : -1;
                        
                        difPsoe = (psoe2021Exist != -1 ? psoe2021Exist.par_percent : 0) - (psoe2019Exist != -1 ? psoe2019Exist.par_percent : 0);
                        item.psoe2021 = psoe2021Exist != -1 ? psoe2021Exist.par_percent : 0;                        
                        item.changePsoe = difPsoe;

                        //Cambio PP
                        let pp2021Exist = 0, pp2019Exist = 0, difPp = 0;

                        pp2021Exist = item.api2021.parties ? 
                                        item.api2021.parties.filter(item => item.par_meta_id == 38)[0] ? 
                                        item.api2021.parties.filter(item => item.par_meta_id == 38)[0] : -1 : -1;

                        pp2019Exist = item.api2019.parties ? 
                                        item.api2019.parties.filter(item => item.par_meta_id == 38)[0] ? 
                                        item.api2019.parties.filter(item => item.par_meta_id == 38)[0] : -1 : -1;
                        
                        difPp = (pp2021Exist != -1 ? pp2021Exist.par_percent : 0) - (pp2019Exist != -1 ? pp2019Exist.par_percent : 0);
                        item.pp2021 = pp2021Exist != -1 ? pp2021Exist.par_percent : 0;
                        item.changePp = difPp;

                        //Cambio CS
                        let cs2021Exist = 0, cs2019Exist = 0, difCs = 0;

                        cs2021Exist = item.api2021.parties ? 
                                        item.api2021.parties.filter(item => item.par_meta_id == 10)[0] ? 
                                        item.api2021.parties.filter(item => item.par_meta_id == 10)[0] : -1 : -1;

                        cs2019Exist = item.api2019.parties ? 
                                        item.api2019.parties.filter(item => item.par_meta_id == 10)[0] ? 
                                        item.api2019.parties.filter(item => item.par_meta_id == 10)[0] : -1 : -1;
                        
                        difCs = (cs2021Exist != -1 ? cs2021Exist.par_percent : 0) - (cs2019Exist != -1 ? cs2019Exist.par_percent : 0);
                        item.cs2021 = cs2021Exist != -1 ? cs2021Exist.par_percent : 0;
                        item.changeCs = difCs;

                        //Cambio MM
                        let mm2021Exist = 0, mm2019Exist = 0, difMm = 0;

                        mm2021Exist = item.api2021.parties ? 
                                        item.api2021.parties.filter(item => item.par_meta_id == 1000091)[0] ? 
                                        item.api2021.parties.filter(item => item.par_meta_id == 1000091)[0] : -1 : -1;

                        mm2019Exist = item.api2019.parties ? 
                                        item.api2019.parties.filter(item => item.par_meta_id == 1000091)[0] ? 
                                        item.api2019.parties.filter(item => item.par_meta_id == 1000091)[0] : -1 : -1;
                        
                        difMm = (mm2021Exist != -1 ? mm2021Exist.par_percent : 0) - (mm2019Exist != -1 ? mm2019Exist.par_percent : 0);
                        item.mm2021 = mm2021Exist != -1 ? mm2021Exist.par_percent : 0;
                        item.changeMm = difMm;

                        //Cambio UP
                        let up2021Exist = 0, up2019Exist = 0, difUp = 0;

                        up2021Exist = item.api2021.parties ? 
                                        item.api2021.parties.filter(item => item.par_meta_id == 64)[0] ? 
                                        item.api2021.parties.filter(item => item.par_meta_id == 64)[0] : -1 : -1;

                        up2019Exist = item.api2019.parties ? 
                                        item.api2019.parties.filter(item => item.par_meta_id == 64)[0] ? 
                                        item.api2019.parties.filter(item => item.par_meta_id == 64)[0] : -1 : -1;
                        
                        difUp = (up2021Exist != -1 ? up2021Exist.par_percent : 0) - (up2019Exist != -1 ? up2019Exist.par_percent : 0);
                        item.up2021 = up2021Exist != -1 ? up2021Exist.par_percent : 0;
                        item.changeUp = difUp;

                        //Cambio VOX
                        let vox2021Exist = 0, vox2019Exist = 0, difVox = 0;

                        vox2021Exist = item.api2021.parties ? 
                                        item.api2021.parties.filter(item => item.par_meta_id == 52)[0] ? 
                                        item.api2021.parties.filter(item => item.par_meta_id == 52)[0] : -1 : -1;

                        vox2019Exist = item.api2019.parties ? 
                                        item.api2019.parties.filter(item => item.par_meta_id == 52)[0] ? 
                                        item.api2019.parties.filter(item => item.par_meta_id == 52)[0] : -1 : -1;
                        
                        difVox = (vox2021Exist != -1 ? vox2021Exist.par_percent : 0) - (vox2019Exist != -1 ? vox2019Exist.par_percent : 0);
                        item.vox2021 = vox2021Exist != -1 ? vox2021Exist.par_percent : 0;
                        item.changeVox = difVox;
                    }
                }                

                //Comprobación y registro de bloques ideológicos en 2019
                if(item.api2019){
                    item.participation2019 = item.api2019["participation "].par_percent_total_votes;

                    if(item.api2019.parties.length > 0) {
                        ////// BLOQUE IDEOLOGICO //////
                        let izquierdaPorc2019 = 0, derechaPorc2019 = 0, restoPorc2019 = 0;
                        for (let i = 0; i < item.api2019.parties.length; i++) {
                            let party = item.api2019.parties[i];
    
                            let tipoBloque = superPartyBlockId[party.par_meta_id] ? superPartyBlockId[party.par_meta_id].bloque : 'sin-bloque';
    
                            if (tipoBloque == 'izquierda') {
                                izquierdaPorc2019 += party.par_percent;
                            } else if (tipoBloque == 'derecha') {
                                derechaPorc2019 += party.par_percent;
                            } else {
                                restoPorc2019 += party.par_percent;
                            }
    
                            item.bloqueGanador2019 = izquierdaPorc2019 > derechaPorc2019 ? 'izquierda' : izquierdaPorc2019 < derechaPorc2019 ? 'derecha' : 'empate';
                            item.izquierdaPorc2019 = izquierdaPorc2019;
                            item.derechaPorc2019 = derechaPorc2019;
                            item.restoPorc2019 = restoPorc2019;
                        }
                        ///// Partidos
                        let psoe2019Exist = item.api2019.parties ? 
                            item.api2019.parties.filter(item => item.par_meta_id == 40)[0] ? 
                            item.api2019.parties.filter(item => item.par_meta_id == 40)[0] : -1 : -1;
                        item.psoe2019 = psoe2019Exist != -1 ? psoe2019Exist.par_percent : 0;
    
                        let pp2019Exist = item.api2019.parties ? 
                            item.api2019.parties.filter(item => item.par_meta_id == 38)[0] ? 
                            item.api2019.parties.filter(item => item.par_meta_id == 38)[0] : -1 : -1;
                        item.pp2019 = pp2019Exist != -1 ? pp2019Exist.par_percent : 0;
    
                        let cs2019Exist = item.api2019.parties ? 
                            item.api2019.parties.filter(item => item.par_meta_id == 10)[0] ? 
                            item.api2019.parties.filter(item => item.par_meta_id == 10)[0] : -1 : -1;
                        item.cs2019 = cs2019Exist != -1 ? cs2019Exist.par_percent : 0;
    
                        let mm2019Exist = item.api2019.parties ? 
                            item.api2019.parties.filter(item => item.par_meta_id == 1000091)[0] ? 
                            item.api2019.parties.filter(item => item.par_meta_id == 1000091)[0] : -1 : -1;
                        item.mm2019 = mm2019Exist != -1 ? mm2019Exist.par_percent : 0;
    
                        let up2019Exist = item.api2019.parties ? 
                            item.api2019.parties.filter(item => item.par_meta_id == 64)[0] ? 
                            item.api2019.parties.filter(item => item.par_meta_id == 64)[0] : -1 : -1;
                        item.up2019 = up2019Exist != -1 ? up2019Exist.par_percent : 0;
    
                        let vox2019Exist = item.api2019.parties ? 
                            item.api2019.parties.filter(item => item.par_meta_id == 52)[0] ? 
                            item.api2019.parties.filter(item => item.par_meta_id == 52)[0] : -1 : -1;
                        item.vox2019 = vox2019Exist != -1 ? vox2019Exist.par_percent : 0;
                    }
                }
                             
            });

            currentDataVisible = 'data_2021';
        
            zoom = zoomInit().scaleExtent([1, 4]).translateExtent([[0,0], [mapWidth,350]]).on('zoom', zoomed);
            svg.call(zoom);
        
            g = svg
                .append('g')
                .attr('id', 'gmap');
            
            //Estado inicial > Primera fuerza de 2021
            g.selectAll("path")
                .data(mapFeatures)
                .enter()
                .append("path")
                .attr("d", path)
                .attr("class", 'madrid-mun')
                .attr("id", function(d){ return d.id; })
                .style("fill", function(d){
                    if(d.api2021 && d.api2021.parties){
                        //Intervalos de color para cada rango porcentual (y, luego, candidatura)
                        let candidatura = d.api2021.parties[0].par_meta_id;
                        if(d.api2021.parties[0].par_percent < 25){
                            return superPartyBlockId[candidatura].lowColor;
                        } else if (d.api2021.parties[0].par_percent >= 25 && d.api2021.parties[0].par_percent < 30) {
                            return superPartyBlockId[candidatura].lowIntermediateColor;
                        } else if (d.api2021.parties[0].par_percent >= 30 && d.api2021.parties[0].par_percent < 35) {
                            return superPartyBlockId[candidatura].upperIntermediateColor;
                        } else {
                            return superPartyBlockId[candidatura].upperColor;
                        }
                    } else {
                        return WITHOUTDATA;
                    }
                })
                .style('opacity', 1)
                .style("stroke", "#fff")
                .style("stroke-width", "0.5px")
                .on('click', function(){
                    zoomCity(this.id);
                });
        });
    }

    async function uploadGeneralProps() {
        //Llamada API
        window.fetch(`https://api.elconfidencial.com/service/electionsscytl/place/${CURRENTELECTION_ID}/CA13/`)
            .then(function(response){
                if(response.ok) {
                    return response.json();
                } else {
                    throw new Error("hola2");
                }                
            }).then(function(data){
                let electionProps = data.data.places.CA13;
                participacion = electionProps.participation.par_percent_total_votes ? electionProps.participation.par_percent_total_votes : 0;
                escrutado = electionProps.participation.par_percent_counted ? electionProps.participation.par_percent_counted : 0;
                document.getElementById('porcParticipacion').textContent = participacion.toString().replace(/\./g, ',') + '%';
                document.getElementById('porcEscrutado').textContent = escrutado.toString().replace(/\./g, ',') + '%';

                document.getElementById('barraParticipacion').style.width = participacion + '%';
                document.getElementById('barraEscrutado').style.width = escrutado  + '%';
            });        
    }

    /*
    * Helpers para inicialización de elementos
    */
    function initData() {
        /*
        * TOOLTIP
        */
        tooltip = select('#map')
            .append('div')
            .attr('class', 'madrid-tooltip')
            .style("opacity", 0);
   
       /*
       * AUTOCOMPLETADO
       */
       initAutocomplete(arrayNombreMunicipios);
    }

    function initEvents() {
        //Lógica para los estados de botones
        document.getElementById('year2021').addEventListener('click', function(e){
            //Cambio de estado de variable
            currentFilter = 'year2021';
            
            //Cambio de estilos para este e.target y/o bloque de botones
            setFilter(e.target.id);
            setLegend();

            //Cambio de lógica en función del botón seleccionado
            if (currentButton == 'primeraFuerza') {
                drawPrimeraFuerza2021();
            } else if (currentButton == 'segundaFuerza') {
                drawSegundaFuerza2021();
            } else if (currentButton == 'bloque-ganador') {
                drawBloqueGanador2021();
            }

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('year2019').addEventListener('click', function(e){
            //Cambio de estado de variable
            currentFilter = 'year2019';

            //Cambio de estilos para este e.target y/o bloque de botones
            setFilter(e.target.id);
            setLegend();
            //Cambio de lógica en función del botón seleccionado
            if(currentButton == 'primeraFuerza') {
                drawPrimeraFuerza2019();
            } else if (currentButton == 'segundaFuerza') {
                drawSegundaFuerza2019();
            } else if (currentButton == 'bloque-ganador') {
                drawBloqueGanador2019();
            }

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('compareYears').addEventListener('change', function(e) {
            let year = e.target.checked ? '2019' : '2021';
            
            if(year == '2021') {
                //Cambio de estado de variable
                currentFilter = 'dif2021';

                //Cambio de estilos para este e.target y/o bloque de botones
                setFilter(currentFilter);
                setLegend(currentButton);

                //Cambio de lógica en función del botón seleccionado
                if(currentButton == 'psoe') {
                    drawPsoe2021();
                } else if (currentButton == 'masmadrid') {
                    drawMasMadrid2021();
                } else if (currentButton == 'vox') {
                    drawVox2021();
                } else if (currentButton == 'pp') {
                    drawPP2021();
                } else if (currentButton == 'cs') {
                    drawCS2021();
                } else if (currentButton == 'podemos') {
                    drawPodemos2021();
                } else if (currentButton == 'participacion') {
                    drawParticipacion2021();
                }

                //Tooltip si hay municipio seleccionado
                if(citySelected){
                    setTooltip(citySelected);
                }
            } else {
                //Cambio de estado de variable
                currentFilter = 'dif2019';
                currentGradient = 'diferencias-2019';

                //Cambio de estilos para este e.target y/o bloque de botones
                setFilter(currentFilter);
                setLegend(currentGradient);

                //Cambio de lógica en función del botón seleccionado
                if(currentButton == 'psoe') {
                    drawPsoeChange();
                } else if (currentButton == 'masmadrid') {
                    drawMasMadridChange();
                } else if (currentButton == 'vox') {
                    drawVoxChange();
                } else if (currentButton == 'pp') {
                    drawPPChange();
                } else if (currentButton == 'cs') {
                    drawCSChange();
                } else if (currentButton == 'podemos') {
                    drawPodemosChange();
                } else if (currentButton == 'participacion') {
                    drawParticipacionChange();
                }

                //Tooltip si hay municipio seleccionado
                if(citySelected){
                    setTooltip(citySelected);
                }               
            }
        });

        //Lógica para los botones de filtros y partidos
        document.getElementById('primeraFuerza').addEventListener('click', function(e) {
            if(currentFilter == 'year2019'){
                drawPrimeraFuerza2019();
                setFilter('year2019');
            } else {
                drawPrimeraFuerza2021();
                setFilter('year2021');
            }
            setBlock('year');
            setButton(e.target);
            setLegend();

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('segundaFuerza').addEventListener('click', function(e) {
            if(currentFilter == 'year2019'){
                drawSegundaFuerza2019();
                setFilter('year2019');
            } else {
                drawSegundaFuerza2021();
                setFilter('year2021');
            }
            
            setBlock('year');
            setButton(e.target);
            setLegend();

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('bloqueGanador').addEventListener('click', function(e) {
            if(currentFilter == 'year2019'){
                drawBloqueGanador2019();
                setFilter('year2019');                
            } else {
                drawBloqueGanador2021();
                setFilter('year2021');
            }
            
            setBlock('year');
            setButton(e.target);
            setLegend('bloque-ganador');           

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('participacion').addEventListener('click', function(e) {
            if(currentFilter == 'dif2019' && (currentButton != 'psoe' && currentButton != 'podemos' && currentButton != 'masmadrid' && currentButton != 'vox' && currentButton != 'pp' && currentButton != 'cs')) {
                drawParticipacionChange();
                setFilter('dif2019');
                setLegend('diferencias-2019');
            } else {
                currentFilter = 'dif2021';
                drawParticipacion2021();
                setFilter('dif2021');
                setLegend('participacion');
            }
                
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('psoe').addEventListener('click', function(e) {
            if(currentFilter == 'dif2019' && currentButton != 'participacion') {
                drawPsoeChange();
                setFilter('dif2019');
                setLegend('diferencias-2019');
            } else {
                currentFilter = 'dif2021';
                setFilter('dif2021');
                drawPsoe2021();
                setLegend('psoe');
            }
                
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }                    
        });

        document.getElementById('masmadrid').addEventListener('click', function(e) {
            if(currentFilter == 'dif2019' && currentButton != 'participacion'){
                drawMasMadridChange();
                setFilter('dif2019');
                setLegend('diferencias-2019');
            } else {
                currentFilter = 'dif2021';
                drawMasMadrid2021();
                setFilter('dif2021');
                setLegend('masmadrid');
            }
            
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('pp').addEventListener('click', function(e) {
            if(currentFilter == 'dif2019' && currentButton != 'participacion'){
                drawPPChange();
                setFilter('dif2019');
                setLegend('diferencias-2019');
            } else {
                currentFilter = 'dif2021';
                drawPP2021();
                setFilter('dif2021');
                setLegend('pp');
            }
            
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('cs').addEventListener('click', function(e) {
            if(currentFilter == 'dif2019' && currentButton != 'participacion'){
                drawCSChange();
                setFilter('dif2019');
                setLegend('diferencias-2019');
            } else {
                currentFilter = 'dif2021';
                drawCS2021();
                setFilter('dif2021');
                setLegend('cs');
            }
            
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('podemos').addEventListener('click', function(e) {
            if(currentFilter == 'dif2019' && currentButton != 'participacion'){
                drawPodemosChange();
                setFilter('dif2019');
                setLegend('diferencias-2019');
            } else {
                currentFilter = 'dif2021';
                drawPodemos2021();
                setFilter('dif2021');
                setLegend('podemos');
            }
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });

        document.getElementById('vox').addEventListener('click', function(e) {
            if(currentFilter == 'dif2019' && currentButton != 'participacion'){
                drawVoxChange();
                setFilter('dif2019');
                setLegend('diferencias-2019');
            } else {
                currentFilter = 'dif2021';
                drawVox2021();
                setFilter('dif2021');
                setLegend('vox');
            }
            setBlock('dif');
            setButton(e.target);

            //Tooltip si hay municipio seleccionado
            if(citySelected){
                setTooltip(citySelected);
            }
        });
    }

    function initZoomEvents() {
        ////////
        //// EVENTOS ZOOM
        ////////
        zoomed = function() {            
            let moveType = event.sourceEvent;

            if(moveType && currentZoom >= event.transform.k){
                getOutTooltip();
                currentCityZoomed != '' ? currentCityZoomed.style.strokeWidth = '0.2px' : null;
                currentCityZoomed != '' ? currentCityZoomed.style.stroke = '#fff' : null;
                currentCityZoomed = '', citySelected = '';
            }
            
            svg.selectAll('path').attr('transform', event.transform);
            currentZoom = event.transform.k;
        }
        
        posicionInicial = function(){
            //ESTILOS
            if(currentCityZoomed != ''){
                currentCityZoomed.style.strokeWidth = '0.2px';
                currentCityZoomed.style.stroke = '#fff';
                currentCityZoomed = '';
                citySelected = '';
            }
            //ZOOM
            svg.transition()
                .duration(1000)
                .call(zoom.transform, zoomIdentity.scale(1));
    
            //TOOLTIP
            tooltip
                .transition()     
                .duration(200)
                .style('display', 'none');
        }
            
        zoomCity = function(city) {
            let ciudad = mapFeatures.filter(function(item){if (item.id == city){return item;}})[0];
            let ciudad2 = document.getElementById(`${city}`);

            let aux = select(ciudad2).node();

            document.getElementById('gmap').removeChild(ciudad2);
            document.getElementById('gmap').appendChild(aux);

            aux.addEventListener('click', function(){
                zoomCity(this.id);
            });
                            
            //ESTILOS
            if(currentCityZoomed != ''){
                currentCityZoomed.style.stroke = '#fff';
                currentCityZoomed.style.strokeWidth = '0.2px';
            }

            aux.style.strokeWidth = '0.5px';
            aux.style.stroke = '#000';

            currentCityZoomed = ciudad2;
    
            //ZOOM
            let x = ciudad.properties.centroid[0];
            let y = ciudad.properties.centroid[1];
            let k = 4;
            let translate = [mapWidth / 2 - k * x, mapHeight / 2 - k * y];
            
            svg.transition()
                .duration(1000)
                .call(zoom.transform, zoomIdentity.translate(translate[0],translate[1]).scale(k));

            //Evento del tooltip
            citySelected = ciudad;
            setTooltip(ciudad);
        }
    
        stopped = function() {
            if (event.defaultPrevented) {
                event.stopPropagation();
            } 
        } 
    }

    /* 
    * Seteo de información del mapa 
    */
    let filterBlocks = document.getElementsByClassName('map-filters__item');
    let filterElementsYears = document.getElementsByClassName('btn--filter');
    let filterElementsDifferences = document.getElementById('compareYears');
    let optionsElements = document.getElementsByClassName('btn--option-js');
    let partiesLegend = document.getElementsByClassName('map-info')[0];

    let currentGradient = 'psoe';
    let gradientBar = document.getElementsByClassName('gradient-box__value')[0];

    let maxVoteData = document.getElementById('maxVoteData');
    let minVoteData = document.getElementById('minVoteData');
    
    function setBlock(tipo) {
        for(let i = 0; i < filterBlocks.length; i++){
            let item = filterBlocks[i];
            item.classList.remove('map-filters--active');
        }
        if(tipo == 'year'){            
            filterBlocks[0].classList.add('map-filters--active');
        } else if (tipo == 'dif') {
            filterBlocks[1].classList.add('map-filters--active');
        } else {
            filterBlocks[2].classList.add('map-filters--active');
        }
    }

    function setFilter(btnId) {
        for(let i = 0; i < filterElementsYears.length; i++) {
            let item = filterElementsYears[i];
            if(item.classList.contains('btn--active')){
                item.classList.remove('btn--active');
            }          
        }

        if(btnId == 'year2021' || btnId == 'year2019') {
            let btn = document.getElementById(btnId);
            btn.classList.add('btn--active');
        } else if (btnId == 'dif2021') {
            filterElementsDifferences.checked = false;
        } else if (btnId == 'dif2019') {
            filterElementsDifferences.checked = true;
        } else {
            currentFilter = 'year2021';
        }  
    }

    function setButton(btn) {
        for(let i = 0; i < optionsElements.length; i++){
            let item = optionsElements[i];
            item.classList.remove('btn--active');
        }

        if(btn) {
            btn.classList.add('btn--active');
        }
    }

    function setLegend(party) {
        //Leyenda
        if(party) {
            if(gradientBar.classList.length == 2) {
                gradientBar.classList.remove(gradientBar.classList.item(1));
                gradientBar.classList.add(`gradient-box__value--${party}`);
            } else {
                gradientBar.classList.add(`gradient-box__value--${party}`);
            }

            if(!partiesLegend.classList.contains('map-info--active')){
                partiesLegend.classList.add('map-info--active');
            }            
        } else if (!party && currentButton == 'bloque-ganador') {
            if(gradientBar.classList.length == 2) {
                gradientBar.classList.remove(gradientBar.classList.item(1));
                gradientBar.classList.add(`gradient-box__value--${currentButton}`);
            } else {
                gradientBar.classList.add(`gradient-box__value--${currentButton}`);
            }

            if(!partiesLegend.classList.contains('map-info--active')){
                partiesLegend.classList.add('map-info--active');
            }
        } else {
            partiesLegend.classList.remove('map-info--active');
            gradientBar.classList.remove(`gradient-box__value--${party}`);
        }
        //Nota al pie de mapa
        if(currentButton == 'bloque-ganador') {
            document.getElementsByClassName('note--bloques-js')[0].classList.add('visible');
        } else {
            document.getElementsByClassName('note--bloques-js')[0].classList.remove('visible');
        }
    }

    /*
    * Helpers de visualización del mapa 
    */
    function drawPrimeraFuerza2021() {
        currentButton = 'primeraFuerza';
        currentDataVisible = 'data_2021';

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.api2021 && d.api2021.parties){
                    //Intervalos de color para cada rango porcentual (y, luego, candidatura)
                    let candidatura = d.api2021.parties[0].par_meta_id;
                    if(d.api2021.parties[0].par_percent < 25){
                        return superPartyBlockId[candidatura].lowColor;
                    } else if (d.api2021.parties[0].par_percent >= 25 && d.api2021.parties[0].par_percent < 30) {
                        return superPartyBlockId[candidatura].lowIntermediateColor;
                    } else if (d.api2021.parties[0].par_percent >= 30 && d.api2021.parties[0].par_percent < 35) {
                        return superPartyBlockId[candidatura].upperIntermediateColor;
                    } else {
                        return superPartyBlockId[candidatura].upperColor;
                    }
                } else {
                    return WITHOUTDATA;
                }  
            });
    }

    function drawPrimeraFuerza2019() {
        currentButton = 'primeraFuerza';
        currentDataVisible = 'data_2019';

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.api2019.parties){
                    //Intervalos de color para cada rango porcentual (y, luego, candidatura)
                    let candidatura = d.api2019.parties[0].par_meta_id;
                    if(d.api2019.parties[0].par_percent < 25){
                        return superPartyBlockId[candidatura].lowColor;
                    } else if (d.api2019.parties[0].par_percent >= 25 && d.api2019.parties[0].par_percent < 30) {
                        return superPartyBlockId[candidatura].lowIntermediateColor;
                    } else if (d.api2019.parties[0].par_percent >= 30 && d.api2019.parties[0].par_percent < 35) {
                        return superPartyBlockId[candidatura].upperIntermediateColor;
                    } else {
                        return superPartyBlockId[candidatura].upperColor;
                    }
                } else {
                    return WITHOUTDATA;
                }
            });
    }

    function drawSegundaFuerza2021() {
        currentButton = 'segundaFuerza';
        currentDataVisible = 'data_2021';

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.api2021 && d.api2021.parties && d.api2021.parties[1]){
                    //Intervalos de color para cada rango porcentual (y, luego, candidatura)
                    let candidatura = d.api2021.parties[1].par_meta_id;
                    if(d.api2021.parties[1].par_percent < 20){
                        return superPartyBlockId[candidatura].lowColor;
                    } else if (d.api2021.parties[1].par_percent >= 20 && d.api2021.parties[1].par_percent < 22.5) {
                        return superPartyBlockId[candidatura].lowIntermediateColor;
                    } else if (d.api2021.parties[1].par_percent >= 22.5 && d.api2021.parties[1].par_percent < 25) {
                        return superPartyBlockId[candidatura].upperIntermediateColor;
                    } else {
                        return superPartyBlockId[candidatura].upperColor;
                    }
                } else {
                    return WITHOUTDATA;
                }
            });
    }

    function drawSegundaFuerza2019() {
        currentButton = 'segundaFuerza';
        currentDataVisible = 'data_2019';

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.api2019.parties && d.api2019.parties[1]){
                    //Intervalos de color para cada rango porcentual (y, luego, candidatura)
                    let candidatura = d.api2019.parties[1].par_meta_id;
                    if(d.api2019.parties[1].par_percent < 20){
                        return superPartyBlockId[candidatura].lowColor;
                    } else if (d.api2019.parties[1].par_percent >= 20 && d.api2019.parties[1].par_percent < 22.5) {
                        return superPartyBlockId[candidatura].lowIntermediateColor;
                    } else if (d.api2019.parties[1].par_percent >= 22.5 && d.api2019.parties[1].par_percent < 25) {
                        return superPartyBlockId[candidatura].upperIntermediateColor;
                    } else {
                        return superPartyBlockId[candidatura].upperColor;
                    }
                } else {
                    return WITHOUTDATA;
                }
            });
    }

    function drawBloqueGanador2021() {
        currentButton = 'bloque-ganador';
        currentDataVisible = 'data_2021';

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if (d.bloqueGanador2021) {
                    let bloqueGanador = d.bloqueGanador2021;
                    let porcGanador = bloqueGanador == 'izquierda' ? d.izquierdaPorc2021 : bloqueGanador == 'derecha' ? d.derechaPorc2021 : null;

                    if (porcGanador) {
                        if (porcGanador < 52) {
                            return GRADIENT_IDEOLOGY[bloqueGanador].first;
                        } else if (porcGanador >= 52 && porcGanador < 56) {
                            return GRADIENT_IDEOLOGY[bloqueGanador].second;
                        } else if (porcGanador >= 56 && porcGanador < 60) {
                            return GRADIENT_IDEOLOGY[bloqueGanador].third;
                        } else if (porcGanador >= 60 && porcGanador < 64) {
                            return GRADIENT_IDEOLOGY[bloqueGanador].fourth;
                        } else if (porcGanador >= 64 && porcGanador < 68) {
                            return GRADIENT_IDEOLOGY[bloqueGanador].fifth;
                        } else {
                            return GRADIENT_IDEOLOGY[bloqueGanador].sixth;
                        }
                    } else {
                        //Contemplar empate
                        return DRAW;
                    }
                } else {
                    return WITHOUTDATA;
                }
            });

        minVoteData.textContent = '+ Dcha.';
        maxVoteData.textContent = '+ Izda.';
    }

    function drawBloqueGanador2019() {
        currentButton = 'bloque-ganador';
        currentDataVisible = 'data_2019';

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if (d.bloqueGanador2019) {
                    let bloqueGanador = d.bloqueGanador2019;
                    let porcGanador = bloqueGanador == 'izquierda' ? d.izquierdaPorc2019 : bloqueGanador == 'derecha' ? d.derechaPorc2019 : null;

                    if (porcGanador) {
                        if (porcGanador < 52) {
                            return GRADIENT_IDEOLOGY[bloqueGanador].first;
                        } else if (porcGanador >= 52 && porcGanador < 56) {
                            return GRADIENT_IDEOLOGY[bloqueGanador].second;
                        } else if (porcGanador >= 56 && porcGanador < 60) {
                            return GRADIENT_IDEOLOGY[bloqueGanador].third;
                        } else if (porcGanador >= 60 && porcGanador < 64) {
                            return GRADIENT_IDEOLOGY[bloqueGanador].fourth;
                        } else if (porcGanador >= 64 && porcGanador < 68) {
                            return GRADIENT_IDEOLOGY[bloqueGanador].fifth;
                        } else {
                            return GRADIENT_IDEOLOGY[bloqueGanador].sixth;
                        }
                    } else {
                        //Contemplar empate
                        return DRAW;
                    }
                } else {
                    return WITHOUTDATA;
                }
            });

        minVoteData.textContent = '+ Dcha.';
        maxVoteData.textContent = '+ Izda.';
    }

    function drawParticipacion2021() {
        currentButton = 'participacion';
        currentDataVisible = 'data_2021';
        
        let colorParticipacion = scaleLinear().domain([0,100]).range(['#e2f3fa','#0a202f']);

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                let datoParticipacion = 0;
                if(d.api2021) {
                    datoParticipacion = d.api2021["participation "] ? d.api2021["participation "].par_percent_total_votes : 0;
                }
                return datoParticipacion == 0 ? WITHOUTDATA : colorParticipacion(datoParticipacion);
            });
        
        minVoteData.textContent = '0%';
        maxVoteData.textContent = '100%';
    }

    function drawParticipacionChange() {
        currentButton = 'participacion';
        currentDataVisible = 'dif_2019';

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.api2021){
                    if (d.difParticipacion < -10) {
                        return CHANGEDOWN_PARTIES[0];
                    } else if (d.difParticipacion >= -10 && d.difParticipacion < -5) {
                        return CHANGEDOWN_PARTIES[1];
                    } else if (d.difParticipacion >= -5 && d.difParticipacion < 0) {
                        return CHANGEDOWN_PARTIES[2];
                    } else if (d.difParticipacion == 0) {
                        return DRAW;
                    } else if (d.difParticipacion > 0 && d.difParticipacion < 5) {
                        return CHANGEUP_PARTIES[0];
                    } else if (d.difParticipacion >= 5 && d.difParticipacion < 10) {
                        return CHANGEUP_PARTIES[1];
                    } else {
                        return CHANGEUP_PARTIES[2];
                    }
                } else {
                    return WITHOUTDATA;
                }
            });

        minVoteData.textContent = '< -10';
        maxVoteData.textContent = '> 10';
    }

    function drawPsoe2021() {
        currentButton = 'psoe';
        currentDataVisible = 'data_2021';        
            
        let colorPsoe = scaleLinear().domain([0,25]).range([superPartyBlockId[40].lightColor,superPartyBlockId[40].color]);

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if (d.api2021 && d.api2021.parties) {
                    let datoPsoe = d.api2021.parties.filter(item => item.par_meta_id == 40)[0];
                    datoPsoe = datoPsoe ? datoPsoe.par_percent : 0;
                    return colorPsoe(datoPsoe);
                } else {
                    return WITHOUTDATA; 
                }
            });

        maxVoteData.textContent = '> 25%';
        minVoteData.textContent = '0 %';
    }

    function drawPsoeChange() {
        currentButton = 'psoe';
        currentDataVisible = 'dif_2019';           

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.changePsoe || d.changePsoe == 0) {
                    if(d.changePsoe < -10) {
                        return CHANGEDOWN_PARTIES[0];
                    } else if (d.changePsoe >= -10 && d.changePsoe < -5) {
                        return CHANGEDOWN_PARTIES[1];
                    } else if (d.changePsoe >= -5 && d.changePsoe < 0) {
                        return CHANGEDOWN_PARTIES[2];
                    } else if (d.changePsoe == 0.0) {
                        return DRAW;
                    } else if (d.changePsoe >= 0.01 && d.changePsoe < 5) {
                        return CHANGEUP_PARTIES[0];
                    } else if (d.changePsoe >= 5 && d.changePsoe < 10) {
                        return CHANGEUP_PARTIES[1];
                    } else {
                        return CHANGEUP_PARTIES[2];
                    }
                } else {
                    return WITHOUTDATA;
                }
            });

        minVoteData.textContent = '< -10';
        maxVoteData.textContent = '> 10';
    }

    function drawMasMadrid2021() {
        currentButton = 'masmadrid';
        currentDataVisible = 'data_2021';
            
        let colorMasMadrid = scaleLinear().domain([0,10]).range([superPartyBlockId[1000091].lightColor,superPartyBlockId[1000091].color]);

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if (d.api2021 && d.api2021.parties) {
                    let datoMm = d.api2021.parties.filter(item => item.par_meta_id == 1000091)[0];
                    datoMm = datoMm ? datoMm.par_percent : 0;
                    return colorMasMadrid(datoMm);
                } else {
                    return WITHOUTDATA; 
                }
            });
        
        minVoteData.textContent = '0 %';
        maxVoteData.textContent = '> 10%';
    }

    function drawMasMadridChange() {
        currentButton = 'masmadrid';
        currentDataVisible = 'dif_2019';           

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.changeMm || d.changeMm == 0) {
                    if(d.changeMm < -10) {
                        return CHANGEDOWN_PARTIES[0];
                    } else if (d.changeMm >= -10 && d.changeMm < -5) {
                        return CHANGEDOWN_PARTIES[1];
                    } else if (d.changeMm >= -5 && d.changeMm < 0) {
                        return CHANGEDOWN_PARTIES[2];
                    } else if (d.changeMm == 0.0) {
                            return DRAW;                        
                    } else if (d.changeMm >= 0.01 && d.changeMm < 5) {
                        return CHANGEUP_PARTIES[0];
                    } else if (d.changeMm >= 5 && d.changeMm < 10) {
                        return CHANGEUP_PARTIES[1];
                    } else {
                        return CHANGEUP_PARTIES[2];
                    }
                } else {
                    return WITHOUTDATA;
                }
            });

        minVoteData.textContent = '< -10';
        maxVoteData.textContent = '> 10';
    }

    function drawPP2021() {
        currentButton = 'pp';
        currentDataVisible = 'data_2021';
            
        let colorPp = scaleLinear().domain([0,30]).range([superPartyBlockId[38].lightColor,superPartyBlockId[38].color]);

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if (d.api2021 && d.api2021.parties) {
                    let datoPp = d.api2021.parties.filter(item => item.par_meta_id == 38)[0];
                    datoPp = datoPp ? datoPp.par_percent : 0;
                    return colorPp(datoPp);
                } else {
                    return WITHOUTDATA; 
                }
            });
        
        minVoteData.textContent = '0 %';
        maxVoteData.textContent = '> 30%';
    }

    function drawPPChange() {
        currentButton = 'pp';
        currentDataVisible = 'dif_2019';           

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.changePp || d.changePp == 0) {
                    if(d.changePp < -10) {
                        return CHANGEDOWN_PARTIES[0];
                    } else if (d.changePp >= -10 && d.changePp < -5) {
                        return CHANGEDOWN_PARTIES[1];
                    } else if (d.changePp >= -5 && d.changePp < 0) {
                        return CHANGEDOWN_PARTIES[2];
                    } else if (d.changePp == 0) {
                        return DRAW;
                    } else if (d.changePp >= 0.01 && d.changePp < 5) {
                        return CHANGEUP_PARTIES[0];
                    } else if (d.changePp >= 5 && d.changePp < 10) {
                        return CHANGEUP_PARTIES[1];
                    } else {
                        return CHANGEUP_PARTIES[2];
                    }
                } else {
                    return WITHOUTDATA;
                }
            });

        minVoteData.textContent = '< -10';
        maxVoteData.textContent = '> 10';
    }

    function drawPodemos2021() {
        currentButton = 'podemos';
        currentDataVisible = 'data_2021';
            
        let colorPodemos = scaleLinear().domain([0,10]).range([superPartyBlockId[64].lightColor,superPartyBlockId[64].color]);

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if (d.api2021 && d.api2021.parties) {
                    let datoUp = d.api2021.parties.filter(item => item.par_meta_id == 64)[0];
                    datoUp = datoUp ? datoUp.par_percent : 0;
                    return colorPodemos(datoUp);
                } else {
                    return WITHOUTDATA; 
                }
            });

        minVoteData.textContent = '0 %';
        maxVoteData.textContent= '+ 10%';
    }

    function drawPodemosChange() {
        currentButton = 'podemos';
        currentDataVisible = 'dif_2019';           

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.changeUp || d.changeUp == 0) {
                    if(d.changeUp < -10) {
                        return CHANGEDOWN_PARTIES[0];
                    } else if (d.changeUp >= -10 && d.changeUp < -5) {
                        return CHANGEDOWN_PARTIES[1];
                    } else if (d.changeUp >= -5 && d.changeUp < 0) {
                        return CHANGEDOWN_PARTIES[2];
                    } else if (d.changeUp == 0) {
                        return DRAW;                        
                    }  else if (d.changeUp >= 0.01 && d.changeUp < 5) {
                        return CHANGEUP_PARTIES[0];
                    } else if (d.changeUp >= 5 && d.changeUp < 10) {
                        return CHANGEUP_PARTIES[1];
                    } else {
                        return CHANGEUP_PARTIES[2];
                    }
                } else {
                    return WITHOUTDATA;
                }
            });

        minVoteData.textContent = '< -10';
        maxVoteData.textContent = '> 10';
    }

    function drawCS2021() {
        currentButton = 'cs';
        currentDataVisible = 'data_2021';
            
        let colorCs = scaleLinear().domain([0,10]).range([superPartyBlockId[10].lightColor,superPartyBlockId[10].color]);

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if (d.api2021 && d.api2021.parties) {
                    let datoCs = d.api2021.parties.filter(item => item.par_meta_id == 10)[0];
                    datoCs = datoCs ? datoCs.par_percent : 0;
                    return colorCs(datoCs);
                } else {
                    return WITHOUTDATA; 
                }
            });

        minVoteData.textContent = '0 %';
        maxVoteData.textContent = '+ 10%';
    }

    function drawCSChange() {
        currentButton = 'cs';
        currentDataVisible = 'dif_2019';           

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.changeCs || d.changeCs == 0) {
                    if(d.changeCs < -10) {
                        return CHANGEDOWN_PARTIES[0];
                    } else if (d.changeCs >= -10 && d.changeCs < -5) {
                        return CHANGEDOWN_PARTIES[1];
                    } else if (d.changeCs >= -5 && d.changeCs < 0) {
                        return CHANGEDOWN_PARTIES[2];
                    } else if (d.changeCs == 0.0) {
                        return DRAW;                        
                    }  else if (d.changeCs >= 0.01 && d.changeCs < 5) {
                        return CHANGEUP_PARTIES[0];
                    } else if (d.changeCs >= 5 && d.changeCs < 10) {
                        return CHANGEUP_PARTIES[1];
                    } else {
                        return CHANGEUP_PARTIES[2];
                    }
                } else {
                    return WITHOUTDATA;
                }
            });
        
        minVoteData.textContent = '< -10';
        maxVoteData.textContent = '> 10';
    }

    function drawVox2021() {
        currentButton = 'vox';
        currentDataVisible = 'data_2021';
            
        let colorVox = scaleLinear().domain([0,10]).range([superPartyBlockId[52].lightColor,superPartyBlockId[52].color]);

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if (d.api2021 && d.api2021.parties) {
                    let datoVox = d.api2021.parties.filter(item => item.par_meta_id == 52)[0];
                    datoVox = datoVox ? datoVox.par_percent : 0;
                    return colorVox(datoVox);
                } else {
                    return WITHOUTDATA; 
                }
            });

        minVoteData.textContent = '0 %';
        maxVoteData.textContent = '+ 10%';
    }

    function drawVoxChange() {
        currentButton = 'vox';
        currentDataVisible = 'dif_2019';           

        g.selectAll(".madrid-mun")
            .transition()
            .duration(800)
            .style("fill", function(d){
                if(d.changeVox || d.changeVox == 0) {
                    if(d.changeVox < -10) {
                        return CHANGEDOWN_PARTIES[0];
                    } else if (d.changeVox >= -10 && d.changeVox < -5) {
                        return CHANGEDOWN_PARTIES[1];
                    } else if (d.changeVox >= -5 && d.changeVox < 0) {
                        return CHANGEDOWN_PARTIES[2];
                    } else if (d.changeVox == 0.0) {
                        return DRAW;                        
                    }  else if (d.changeVox >= 0.01 && d.changeVox < 5) {
                        return CHANGEUP_PARTIES[0];
                    } else if (d.changeVox >= 5 && d.changeVox < 10) {
                        return CHANGEUP_PARTIES[1];
                    } else {
                        return CHANGEUP_PARTIES[2];
                    }
                } else {
                    return WITHOUTDATA;
                }
            });

        minVoteData.textContent = '< -10';
        maxVoteData.textContent = '> 10';
    }

    /* Pintado de tooltip */
    function drawTooltip(id, d) {
        //Lógica previa de ordenación de partidos
        let parties = [
            {name: 'PSOE', data21: d.psoe2021, data19: d.psoe2019, dataChange: d.changePsoe},
            {name: 'PP', data21: d.pp2021, data19: d.pp2019, dataChange: d.changePp},
            {name: 'Cs', data21: d.cs2021, data19: d.cs2019, dataChange: d.changeCs},
            {name: 'M. Madrid', data21: d.mm2021, data19: d.mm2019, dataChange: d.changeMm},
            {name: 'VOX', data21: d.vox2021, data19: d.vox2019, dataChange: d.changeVox},
            {name: 'Podemos', data21: d.up2021, data19: d.up2019, dataChange: d.changeUp}
        ]

        //Creación de arrays auxiliares para evitar que las ordenaciones se sobreescriban entre sí
        let aux2021 = [...parties];
        let aux2019 = [...parties];
        let partyOrder2021 = aux2021.sort(comparativa2021);
        let partyOrder2019 = aux2019.sort(comparativa2019);

        //Pintado de tooltips
        let html = '<span id="close-tooltip">x</span>';

        let munWithoutSpace = d.properties.MUNI.trim();
        let firstMun = munWithoutSpace.substr(0,1).toUpperCase();
        let restMun = munWithoutSpace.substr(1,);
        let totalMun = firstMun.concat(restMun);

        html += `<div class="widget__title">${totalMun}</div>`;

        if(id == 'data_2021') {
            if(d.api2021 && d.api2021.parties.length > 0) { //Si hay partidos, por aquí
                html += `
                    <div class="b-fuerzas">
                        <div class="b-fuerzas__title">Resultados en 2021</div>
                        <div class="b-fuerzas__parties">
                            <div>${partyOrder2021[0].name} (${partyOrder2021[0].data21.toFixed(1).toString().replace('.',',')}%)</div>
                            <div>${partyOrder2021[1].name} (${partyOrder2021[1].data21.toFixed(1).toString().replace('.',',')}%)</div>
                            <div>${partyOrder2021[2].name} (${partyOrder2021[2].data21.toFixed(1).toString().replace('.',',')}%)</div>
                            <div>${partyOrder2021[3].name} (${partyOrder2021[3].data21.toFixed(1).toString().replace('.',',')}%)</div>
                            <div>${partyOrder2021[4].name} (${partyOrder2021[4].data21.toFixed(1).toString().replace('.',',')}%)</div>
                            <div>${partyOrder2021[5].name} (${partyOrder2021[5].data21.toFixed(1).toString().replace('.',',')}%)</div>
                        </div>
                    </div>
                    <div class="bloques">
                        <div class="bloques__title">Bloques</div>
                        <div class="bloques__items">
                            <div class="bloque__item">
                                <span>Izda.</span>
                                <div class="bloque__bar" style="width: ${(d.izquierdaPorc2021 * 135) / 100}px; background: #fbdd49;"></div>
                                <span>${d.izquierdaPorc2021.toFixed(1).toString().replace('.',',')}%</span>
                            </div>
                            <div class="bloque__item">
                                <span>Dcha.</span>
                                <div class="bloque__bar" style="width: ${(d.derechaPorc2021 * 135) / 100}px; background: #3c4554;"></div>
                                <span>${d.derechaPorc2021.toFixed(1).toString().replace('.',',')}%</span>
                            </div>
                            <div class="bloque__item">
                                <span>Otros</span>
                                <div class="bloque__bar" style="width: ${(d.restoPorc2021 * 135) / 100}px; background: #cdcdcd; "></div>
                                <span>${d.restoPorc2021.toFixed(1).toString().replace('.',',')}%</span>
                            </div>
                        </div>
                    </div>
                    <div class="participation">
                        <span class="participation__title">Participación</span>
                        <span class="participation__data">${d.participation2021 ? d.participation2021.toFixed(1).toString().replace('.',',') : '0'}% (${d.difParticipacion > 0 ? '+' + d.difParticipacion.toFixed(1).toString().replace('.',',') : d.difParticipacion.toFixed(1).toString().replace('.',',')})</span>
                    </div>
                `;
            } else if (d.api2021) { //Si no hay partidos, mostramos la participación
                html += `
                    <div class="b-fuerzas">
                        <div class="b-fuerzas__title">Resultados en 2021</div>
                        <div style="margin-bottom: 8px; font-size: 12px;">Sin info.</div>
                    </div>
                    <div class="bloques">
                        <div class="bloques__title">Bloques</div>
                        <div style="font-size: 12px;">Sin info.</div>
                    </div>
                    <div class="participation">
                        <span class="participation__title">Participación</span>
                        <span class="participation__data">${d.participation2021 ? d.participation2021.toFixed(1).toString().replace('.',',') : '0'}% (${d.difParticipacion > 0 ? '+' + d.difParticipacion.toFixed(1).toString().replace('.',',') : d.difParticipacion.toFixed(1).toString().replace('.',',')})</span>
                    </div>
                `;
            } else {
                html += `
                    <div class="b-fuerzas">
                        <div class="b-fuerzas__title">Resultados en 2021</div>
                        <div style="margin-bottom: 8px; font-size: 12px;">Sin info.</div>
                    </div>
                    <div class="bloques">
                        <div class="bloques__title">Bloques</div>
                        <div style="font-size: 12px;">Sin info.</div>
                    </div>
                    <div class="participation">
                        <span class="participation__title">Participación</span>
                        <span class="participation__data">Sin info.</span>
                    </div>
                `;
            }
        } else if (id == 'data_2019') {
            html += `
                <div class="b-fuerzas">
                    <div class="b-fuerzas__title">Resultados en 2019</div>
                    <div class="b-fuerzas__parties">
                        <div>${partyOrder2019[0].name} (${partyOrder2019[0].data19.toFixed(1).toString().replace('.',',')}%)</div>
                        <div>${partyOrder2019[1].name} (${partyOrder2019[1].data19.toFixed(1).toString().replace('.',',')}%)</div>
                        <div>${partyOrder2019[2].name} (${partyOrder2019[2].data19.toFixed(1).toString().replace('.',',')}%)</div>
                        <div>${partyOrder2019[3].name} (${partyOrder2019[3].data19.toFixed(1).toString().replace('.',',')}%)</div>
                        <div>${partyOrder2019[4].name} (${partyOrder2019[4].data19.toFixed(1).toString().replace('.',',')}%)</div>
                        <div>${partyOrder2019[5].name} (${partyOrder2019[5].data19.toFixed(1).toString().replace('.',',')}%)</div>
                    </div>
                </div>
                <div class="bloques">
                    <div class="bloques__title">Bloques</div>
                    <div class="bloques__items">
                        <div class="bloque__item">
                            <span>Izda.</span>
                            <div class="bloque__bar" style="width: ${(d.izquierdaPorc2019 * 135) / 100}px; background: #fbdd49;"></div>
                            <span>${d.izquierdaPorc2019.toFixed(1).toString().replace('.',',')}%</span>
                        </div>
                        <div class="bloque__item">
                            <span>Dcha.</span>
                            <div class="bloque__bar" style="width: ${(d.derechaPorc2019 * 135) / 100}px; background: #3c4554;"></div>
                            <span>${d.derechaPorc2019.toFixed(1).toString().replace('.',',')}%</span>
                        </div>
                        <div class="bloque__item">
                            <span>Otros</span>
                            <div class="bloque__bar" style="width: ${(d.restoPorc2019 * 135) / 100}px; background: #cdcdcd; "></div>
                            <span>${d.restoPorc2019.toFixed(1).toString().replace('.',',')}%</span>
                        </div>
                    </div>
                </div>
                <div class="participation">
                    <span class="participation__title">Participación</span>
                    <span class="participation__data">${d.participation2019.toFixed(1).toString().replace('.',',')}%</span>
                </div>
            `;
        } else {
            if(d.api2021 && d.api2021.parties.length > 0) {
                html += `
                    <div class="b-fuerzas">
                        <div class="b-fuerzas__title">Dif. de voto respecto a 2019</div>
                        <div class="b-fuerzas__parties">
                            <div>${partyOrder2021[0].name} (${partyOrder2021[0].dataChange.toFixed(1).toString().replace('.',',') > 0 ? '+' + partyOrder2021[0].dataChange.toFixed(1).toString().replace('.',',') : partyOrder2021[0].dataChange.toFixed(1).toString().replace('.',',')})</div>
                            <div>${partyOrder2021[1].name} (${partyOrder2021[1].dataChange.toFixed(1).toString().replace('.',',') > 0 ? '+' + partyOrder2021[1].dataChange.toFixed(1).toString().replace('.',',') : partyOrder2021[1].dataChange.toFixed(1).toString().replace('.',',')})</div>
                            <div>${partyOrder2021[2].name} (${partyOrder2021[2].dataChange.toFixed(1).toString().replace('.',',') > 0 ? '+' + partyOrder2021[2].dataChange.toFixed(1).toString().replace('.',',') : partyOrder2021[2].dataChange.toFixed(1).toString().replace('.',',')})</div>
                            <div>${partyOrder2021[3].name} (${partyOrder2021[3].dataChange.toFixed(1).toString().replace('.',',') > 0 ? '+' + partyOrder2021[3].dataChange.toFixed(1).toString().replace('.',',') : partyOrder2021[3].dataChange.toFixed(1).toString().replace('.',',')})</div>
                            <div>${partyOrder2021[4].name} (${partyOrder2021[4].dataChange.toFixed(1).toString().replace('.',',') > 0 ? '+' + partyOrder2021[4].dataChange.toFixed(1).toString().replace('.',',') : partyOrder2021[4].dataChange.toFixed(1).toString().replace('.',',')})</div>
                            <div>${partyOrder2021[5].name} (${partyOrder2021[5].dataChange.toFixed(1).toString().replace('.',',') > 0 ? '+' + partyOrder2021[5].dataChange.toFixed(1).toString().replace('.',',') : partyOrder2021[5].dataChange.toFixed(1).toString().replace('.',',')})</div>
                        </div>
                    </div>
                    <div class="participation">
                        <span class="participation__title">Dif. en participación:</span>
                        <span class="participation__data">${d.difParticipacion > 0 ? '+' + d.difParticipacion.toFixed(1).toString().replace('.',',') : d.difParticipacion.toFixed(1).toString().replace('.',',')}</span>
                    </div>
                `;
            } else if (d.api2021) {
                html += `
                    <div class="b-fuerzas">
                        <div class="b-fuerzas__title">Dif. de voto respecto a 2019</div>
                        <div style="margin-bottom: 8px; font-size: 12px;">Sin info.</div>
                    </div>
                    <div class="participation">
                        <span class="participation__title">Dif. en participación:</span>
                        <span class="participation__data">${d.difParticipacion > 0 ? '+' + d.difParticipacion.toFixed(1).toString().replace('.',',') : d.difParticipacion.toFixed(1).toString().replace('.',',')}</span>
                    </div>
                `;
            } else {
                html += `
                    <div class="b-fuerzas">
                        <div class="b-fuerzas__title">Dif. de voto respecto a 2019</div>
                        <div style="margin-bottom: 8px; font-size: 12px;">Sin info.</div>
                    </div>
                    <div class="participation">
                        <span class="participation__title">Dif. en participación:</span>
                        <span class="participation__data">Sin info.</span>
                    </div>
                `;
            }
        }

        return html;
    }

    function setTooltip(d) {
        let result = drawTooltip(currentDataVisible, d);                    
        tooltip.html(result);

        document.getElementById('close-tooltip').addEventListener('click', function () {
            getOutTooltip();
            if(currentCityZoomed != ''){
                currentCityZoomed.style.stroke = '#fff';
                currentCityZoomed.style.strokeWidth = '0.2px';
                currentCityZoomed = '';
                citySelected = '';
                posicionInicial();
            }
        });
        
        showTooltip();
    }

    function getOutTooltip() {
        tooltip
            .transition()     
            .duration(200)
            .style('display', 'none');
    }
    
    function showTooltip() {
        tooltip
            .transition()     
            .duration(200)
            .style('display', 'block')
            .style('opacity','1');
    }
}