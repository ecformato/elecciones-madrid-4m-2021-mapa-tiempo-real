//Uso de librería d3
import { json } from 'd3-request';
import { queue } from 'd3-queue';
import { initAutocomplete } from '../helpers/autocomplete';
import { superPartyBlockId, CURRENTELECTION_ID, FORMERELECTION_ID } from '../helpers/parties';
import { getNumberWithThousandsPoint } from '../helpers/dom-logic';

export let searchCity, reInitState;
export function mobileDevelopment() {
    let q = queue();
    q.defer(json, "https://raw.githubusercontent.com/CarlosMunozDiazEC/prueba-datos/main/batalla_madrid/mun_madrid_data.json");
    q.defer(json, `https://api.elconfidencial.com/service/electionsscytl/place/${CURRENTELECTION_ID}/CA13/?compareTo=${FORMERELECTION_ID}`);

    q.await(function(error, nombreMunicipios, dataTabla) {
        if (error) throw error;

        initAutocomplete(nombreMunicipios);

        const MADRID_DATA = dataTabla;

        //Primero mostramos la información de Madrid por defecto (dataTabla)
        drawResults(dataTabla, 'CA13');

        //Si se hace una búsqueda de un municipio en concreto, cambiamos los valores
        searchCity = function (idMunicipio) {
            //Llamada API
            window.fetch(`https://api.elconfidencial.com/service/electionsscytl/place/${CURRENTELECTION_ID}/${idMunicipio}/?compareTo=${FORMERELECTION_ID}`)
                .then(function(response){
                    return response.json()
                }).then(function(data){
                    drawResults(data, idMunicipio);
                    resetNavigation();
                });
        }

        function drawResults(data, id) {
            let currentResults = data.data.places[id];
            let formerResults = data.data.placePrevious[id];

            //Nombre
            document.getElementById('nombreMobile').textContent = currentResults.pla_name;
            /////////
            //// Bucle para tabla y bloques ideológicos
            ////////
            document.getElementById('tablaMobile').textContent = '';

            //Partidos con escaño
            let auxPartidosConEscano = [];

            //Bloques ideológicos
            let mayoriaBloque = '', auxPorcIzq = 0, auxPorcDer = 0;
            for(let i = 0; i < currentResults.results.length; i++) {
                let item = currentResults.results[i];
                
                //BLOQUE TABLA
                auxPartidosConEscano.push(item);
                let filaPartido = document.createElement('div');
                filaPartido.className = "table__row table__row--content";

                let columnaAlias = document.createElement('div');
                columnaAlias.className = "table__column";
                let aliasPartidoSpan = document.createElement('span');

                let columnaVotos = document.createElement('div');
                columnaVotos.className = "table__column";
                let votosPartidoSpan = document.createElement('span');

                let columnaPorcentajes = document.createElement('div');
                columnaPorcentajes.className = "table__column";
                let porcVotosSpan = document.createElement('span');

                let columnaDiferencia = document.createElement('div');
                columnaDiferencia.className = "table__column";
                let diferenciaPuntosSpan = document.createElement('span');
                let diferenciaPuntosIcon;
                let aliasPartido = superPartyBlockId[item.res_party.par_meta_id].alias;
                aliasPartidoSpan.textContent = aliasPartido;
                aliasPartidoSpan.className = `table__party table__party--${aliasPartido.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[ +]/g , "-").toLowerCase()}`;

                let votosPartido = item.res_votes;
                votosPartidoSpan.textContent = getNumberWithThousandsPoint(votosPartido);
                votosPartidoSpan.className = "table__value";

                let porcVotos = item.res_percent_votes;
                porcVotosSpan.textContent = porcVotos.toString().replace(/\./g, ',');
                porcVotosSpan.className = "table__value";

                let partido2019 = formerResults.results.filter(item2019 => item2019.res_party.par_meta_id == item.res_party.par_meta_id)[0];
                if(partido2019) {
                    let porcVotos2019 = partido2019.res_percent_votes;
                    let diferenciaPorc =  porcVotos - porcVotos2019;
                    diferenciaPuntosSpan.textContent = diferenciaPorc.toFixed(2).toString().replace(/\./g, ',');
                    diferenciaPuntosIcon = document.createElement('img');
                    diferenciaPuntosIcon.setAttribute("width",9);
                    diferenciaPuntosIcon.className = "table__icon";
                    if ( diferenciaPorc < 0 ) {
                        diferenciaPuntosIcon.setAttribute("src","https://www.ecestaticos.com/file/ba9ab8ff150fae6aa699cf903c1f610d/1612914683-icono-baja.svg");
                    } else if ( diferenciaPorc > 0 ) {
                        diferenciaPuntosIcon.setAttribute("src","https://www.ecestaticos.com/file/59138a0f8122a460ea50930c7481d5b2/1612914662-icono-sube.svg");
                    } else {
                        diferenciaPuntosIcon.setAttribute("src","https://www.ecestaticos.com/file/8fcce682e23eb9fa9037de230a36760d/1612914710-icono-igual.svg");
                    }
                } else {
                    columnaDiferencia.className = "table__column table__column--center";
                    diferenciaPuntosSpan.textContent = '-';
                }
                diferenciaPuntosSpan.className = "table__value table__value--dif";

                columnaAlias.appendChild(aliasPartidoSpan);
                columnaVotos.appendChild(votosPartidoSpan);
                columnaPorcentajes.appendChild(porcVotosSpan);
                columnaDiferencia.appendChild(diferenciaPuntosSpan);
                if (typeof diferenciaPuntosIcon !== 'undefined') {
                    columnaDiferencia.appendChild(diferenciaPuntosIcon);
                }

                filaPartido.appendChild(columnaAlias);
                filaPartido.appendChild(columnaVotos);
                filaPartido.appendChild(columnaPorcentajes);
                filaPartido.appendChild(columnaDiferencia);

                document.getElementById('tablaMobile').appendChild(filaPartido);

                //BLOQUE IDEOLÓGICO                
                let tipoBloque = superPartyBlockId[item.res_party.par_meta_id] ? superPartyBlockId[item.res_party.par_meta_id].bloque : null;
                
                if(tipoBloque == 'izquierda') {
                    auxPorcIzq += item.res_percent_votes;
                } else {
                    auxPorcDer += item.res_percent_votes;
                }
            }

            //Tabla > Ubicar las candidaturas restantes
            let lengthPartidosConEscano = auxPartidosConEscano.length;
            
            if(lengthPartidosConEscano < 20) {
                let auxPartidosSinEscano = [];
                Object.entries(superPartyBlockId).forEach(function(superParty) {
                    let idPartido = superParty[0];
                    let partido = auxPartidosConEscano.filter(subItem => subItem.res_party.par_meta_id == idPartido)[0];
                    
                    if(!partido) {
                        auxPartidosSinEscano.push(superParty[1]);
                    }
                });
                
                for(let i = 0; i < auxPartidosSinEscano.length; i++) {
                    let filaPartido = document.createElement('div');
                    filaPartido.className = "table__row table__row--content table__row--disabled";

                    let columnaAlias = document.createElement('div');
                    columnaAlias.className = "table__column";
                    let aliasPartidoSpan = document.createElement('span');

                    let columnaVotos = document.createElement('div');
                    columnaVotos.className = "table__column";
                    let votosPartidoSpan = document.createElement('span');

                    let columnaPorcentajes = document.createElement('div');
                    columnaPorcentajes.className = "table__column";
                    let porcVotosSpan = document.createElement('span');

                    let columnaDiferencia = document.createElement('div');
                    columnaDiferencia.className = "table__column table__column--center";
                    let diferenciaPuntosSpan = document.createElement('span');

                    let aliasPartido = auxPartidosSinEscano[i].alias;
                    aliasPartidoSpan.textContent = aliasPartido;
                    votosPartidoSpan.textContent = '-';
                    porcVotosSpan.textContent = '-';
                    diferenciaPuntosSpan.textContent = '-';    

                    aliasPartidoSpan.className = `table__party table__party--${aliasPartido.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[ +]/g , "-").toLowerCase()}`;
                    votosPartidoSpan.className = "table__value";
                    porcVotosSpan.className = "table__value";
                    diferenciaPuntosSpan.className = "table__value table__value--dif";

                    columnaAlias.appendChild(aliasPartidoSpan);
                    columnaVotos.appendChild(votosPartidoSpan);
                    columnaPorcentajes.appendChild(porcVotosSpan);
                    columnaDiferencia.appendChild(diferenciaPuntosSpan);

                    filaPartido.appendChild(columnaAlias);
                    filaPartido.appendChild(columnaVotos);
                    filaPartido.appendChild(columnaPorcentajes);
                    filaPartido.appendChild(columnaDiferencia);

                    document.getElementById('tablaMobile').appendChild(filaPartido);
                }
            }            

            //Bloques ideológicos
            mayoriaBloque = auxPorcIzq > auxPorcDer ? 'Mayoría de voto <span class="label--izquierda">de izquierdas</span>' : auxPorcIzq < auxPorcDer ? 'Mayoría de voto <span class="label--derecha">de derechas</span>' : 'Empate en voto <span class="label--equal">izq./dcha.</span>';
            document.getElementById('bloqueIdeologicoMobile').innerHTML = mayoriaBloque;
            //Escrutinio
            document.getElementById('escrutinioMobile').textContent = currentResults.participation.par_percent_counted ? 
                currentResults.participation.par_percent_counted.toString().replace(/\./g, ',') + '%' : 'Todavía no hay info.';
            document.getElementById('barraEscrutinioMobile').style.width = currentResults.participation ?
                currentResults.participation.par_percent_counted + '%' : '0%';
            //Participacion 2021
            document.getElementById('participacionMobile').textContent = currentResults.participation.par_percent_total_votes ? 
                currentResults.participation.par_percent_total_votes.toString().replace(/\./g, ',') + '%' : 'Todavía no hay info.';
            document.getElementById('barraParticipacionMobile').style.width = currentResults.participation ?
                currentResults.participation.par_percent_total_votes + '%' : '0%';
        }

        //Vuelta al estado inicial con MADRID
        reInitState = function() {
            drawResults(MADRID_DATA, 'CA13');
            resetNavigation();
        }
    });
    
    //Navegación en tabla de resultados
    let currentVisibleSlide = 1;
    let table = document.getElementById("tablaMobile");
    let tableWidth;
    let arrowPrev = document.getElementById("arrowPrev");
    let arrowNext = document.getElementById("arrowNext");
    let dots = document.getElementsByClassName("dot");

    configureNavigation();

    function configureNavigation() {
        tableWidth = table.offsetWidth;
        addArrowsListeners();
    }

    function resetNavigation() {
        currentVisibleSlide = 1;
        table.style.transform = 'translateX(0)';
        updateArrows();
        updateDots();
    }

    function addArrowsListeners() {
        let arrows = document.getElementsByClassName("arrow");
        for (let i=0; i < arrows.length; i++) {
            arrows[i].addEventListener("click", function(e) {
                e.preventDefault();
                let direction = arrows[i].classList.contains("arrow--prev") ? "prev" : "next";
                if ( ( (direction == "prev") && (currentVisibleSlide != 1) ) || ( (direction == "next") && (currentVisibleSlide != 3) ) ) {
                    updateSlide(direction);
                }
            });
        }
    }

    function updateSlide(dir) {
        currentVisibleSlide = ( dir == "prev" ) ? currentVisibleSlide - 1 : currentVisibleSlide + 1;
        let translation = tableWidth * ( currentVisibleSlide - 1 );
        table.style.transform = `translateX(-${translation}px)`;
        updateArrows();
        updateDots();
    }

    function updateArrows() {
        if ( currentVisibleSlide == 1 ) {
            arrowPrev.classList.add("arrow--disabled");
            arrowNext.classList.remove("arrow--disabled");
        } else if ( currentVisibleSlide == 2 ) {
            if ( arrowPrev.classList.contains("arrow--disabled") ) {
                arrowPrev.classList.remove("arrow--disabled");
            }
            if ( arrowNext.classList.contains("arrow--disabled") ) {
                arrowNext.classList.remove("arrow--disabled");
            }
        } else {
            arrowPrev.classList.remove("arrow--disabled");
            arrowNext.classList.add("arrow--disabled");
        }
    }

    function updateDots() {
        let dotActive = document.getElementsByClassName("dot--active")[0];
        dotActive.classList.remove("dot--active");
        dots[currentVisibleSlide-1].classList.add("dot--active");
    }
}