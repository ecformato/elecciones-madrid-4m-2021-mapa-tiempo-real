export const CURRENTELECTION_ID = '100'; //2021
export const FORMERELECTION_ID = '96'; //2019

export const superPartyBlockId = {
    //Partidos con opciones de sacar escaño
    10: {alias: 'CS', largeAlias: 'CS', lightColor: '#ffc09f', color: '#f45a09', bloque: 'derecha', cssClass: 'table__partido--cs', lowColor: '#f6e9e1', lowIntermediateColor: '#f2be9f', upperIntermediateColor: '#f38c54', upperColor: '#f45a09'}, //Cs
    1000091: {alias: 'Más Madrid', largeAlias: 'Más Madrid', lightColor: '#bae8e4', color: '#20c0b2', bloque: 'izquierda', cssClass: 'table__partido--masmadrid', lowColor: '#ebf4f3', lowIntermediateColor: '#b7ded7', upperIntermediateColor: '#85cdc5', upperColor: '#20c0b2'}, //MM
    38: {alias: 'PP', largeAlias: 'PP', lightColor: '#c6e5ff', color: '#48aafd', bloque: 'derecha', cssClass: 'table__partido--pp', lowColor: '#eff3f6', lowIntermediateColor: '#b9d9f0', upperIntermediateColor: '#80c1f7', upperColor: '#48aafd'}, //PP
    40: {alias: 'PSOE', largeAlias: 'PSOE', lightColor: '#fbc9c7', color: '#f60b01', bloque: 'izquierda', cssClass: 'table__partido--psoe', lowColor: '#f5ebea', lowIntermediateColor: '#f3a49c', upperIntermediateColor: '#f4574f', upperColor: '#f60b01'}, //PSOE
    64: {alias: 'Podemos', largeAlias: 'Podemos', lightColor: '#d7a7dd', color: '#692772', bloque: 'izquierda', cssClass: 'table__partido--podemos', lowColor: '#efeaf1', lowIntermediateColor: '#c4adc2', upperIntermediateColor: '#966a9a', upperColor: '#692772'}, //Podemos
    52: {alias: 'VOX', largeAlias: 'VOX', lightColor: '#d5e9c0', color: '#77be2d', bloque: 'derecha', cssClass: 'table__partido--vox', lowColor: '#ecf2e4', lowIntermediateColor: '#c8dfab', upperIntermediateColor: '#a0cf6c', upperColor: '#77be2d'}, //VOX
    //Resto de candidaturas
    29: {alias: 'PACMA', largeAlias: 'PACMA', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    65: {alias: 'PUM+J', largeAlias: 'PUM+J', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    1000093: {alias: 'PCTE', largeAlias: 'PCTE', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    12: {alias: 'EB', largeAlias: 'Escaños en Blanco', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    4100005: {alias: 'UEP', largeAlias: 'Unión Europea de Pensionistas', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    19: {alias: 'FE JONS', largeAlias: 'FE de las JONS', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    34: {alias: 'P-LIB', largeAlias: 'P. Libertario', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    33: {alias: 'PH', largeAlias: 'P. Humanista', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    1000092: {alias: 'PCAS-TC', largeAlias: 'PCAS-TC', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    724: {alias: '3ª Acción', largeAlias: '3ª en Acción', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    4100012: {alias: 'RC-PCAS-TC', largeAlias: 'Recortes Cero-PCAS-TC-GV-M', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    4100010: {alias: 'PCOE-PCPE', largeAlias: 'PCOE-PCPE', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    4100011: {alias: 'POLE', largeAlias: 'POLE', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    12301: {alias: 'UDEC', largeAlias: 'UDEC', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    4000018: {alias: 'VOLT', largeAlias: 'VOLT', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'},
    4100009: {alias: 'P. AUTÓN.', largeAlias: 'PARTIDO AUTÓNOMO', color: '#575757', bloque: '', cssClass: 'table__partido--recortes', lowColor: '#575757', lowIntermediateColor: '#575757', upperIntermediateColor: '#575757', upperColor: '#575757'}
}