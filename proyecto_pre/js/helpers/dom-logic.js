export function isMobile() {
    return window.innerWidth < 525; //Cambiar cuando sepamos corte (en widget de portada está en 485)
}

export function comparativa2021(a,b){
    const datoA = a.data21;
    const datoB = b.data21;

    let comparacion = 0;
    if (datoA < datoB) {
        comparacion = 1;
    } else if (datoA >= datoB) {
        comparacion = -1;
    }
    return comparacion;
}

export function comparativa2019(a,b){
    const datoA = a.data19;
    const datoB = b.data19;

    let comparacion = 0;
    if (datoA < datoB) {
        comparacion = 1;
    } else if (datoA >= datoB) {
        comparacion = -1;
    }
    return comparacion;
}

export function getNumberWithThousandsPoint(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}